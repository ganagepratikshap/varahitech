﻿import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from '@/_services';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    userDetail:any;
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.authenticationService.currentUserValue;
        this.userDetail=JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser) {   
// console.log("this.userDetail.userrole[0] ",this.userDetail );
            // if(this.userDetail.userrole[0] =='user'){
            //     this.router.navigate(['/test']);
            // }else{
            //     this.router.navigate(['/login']);
            // }
            // authorised so return true
            return true;
        }

        // not logged in so redirect to login page with the return url
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
        return false;
    }
}