export class Employee {
    id: number;
    email: string;
    // password: string;
    firstName: string;
    lastName: string;
    address: string;
    dob:string;
    mobile:number;
    city:string;
    // role:string;
}