﻿export class User {
    id: number;
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    token: string;
    dob:string;
    address:string;
    company:string;
    // role:string;
}