﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User ,Employee} from '@/_models';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>(`http://localhost:4200/users`);
    }
    getAllEmployees() {
        return this.http.get<Employee[]>(`http://localhost:4200/employees`);
    }
    register(user: User) {
        return this.http.post(`http://localhost:4200/users/register`, user);
    }
    createEmp(employee: Employee) {
        return this.http.post(`http://localhost:4200/users/employee`, employee);
    }
    delete(id: number) {
        return this.http.delete(`http://localhost:4200/users/${id}`);
    }
    
    update(employee: Employee) {
        // console.log("id is-----------",id);
        return this.http.put(`http://localhost:4200/Updateusers`,employee);
    }
}