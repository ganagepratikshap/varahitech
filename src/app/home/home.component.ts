﻿import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { AlertService, UserService, AuthenticationService } from '@/_services';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder,Validators } from '@angular/forms';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { ChangeDetectionStrategy, HostListener } from '@angular/core';
@Component({
  templateUrl: 'home.component.html', providers: [
    ConfirmationService]
})
export class HomeComponent implements OnInit {

  title = 'modal2';
  update: boolean;
  emp: any; employees = [];
  oldEmp: []; cols: any[]; totalRecords: number;
  user1: any;
  editProfileForm: FormGroup;
  userListt: any;
  confirmDialogVisible = false;
  loading = false;
  submitted = false;
  constructor(private fb: FormBuilder, private modalService: NgbModal, private userService: UserService, private alertService: AlertService, private confirmationService: ConfirmationService) { }

  ngOnInit() {
    this.editProfileForm = this.fb.group({
      id: [''],
      firstname: ['',Validators.required],
      lastname: ['',Validators.required],
      address: ['',Validators.required],
      email: ['', [Validators.required,Validators.email]],
      dob: ['',Validators.required],
      mob: ['',[Validators.required, Validators.minLength(10)]],
      city: ['',Validators.required]
    
    });
    this.loadAllEmployees();
  }
// convenience getter for easy access to form fields
get f() { return this.editProfileForm.controls; }
  deleteUser(id: number) {
    this.confirmationService.confirm({
      key: 'confirm-check',
      message: 'Are you sure that you want to delete this record?',
      accept: () => {
        this.userService.delete(id)
          .pipe(first())
          .subscribe(() => this.loadAllEmployees());
      }
    });

  }

  private loadAllEmployees() {
    this.userService.getAllEmployees()
      .pipe(first())
      .subscribe(employees => this.employees = employees);
    this.cols = [
      { field: 'firstname', header: 'FirstName' },
      { field: 'lastname', header: 'LastName' },
      { field: 'email', header: 'Email' },
      { field: 'address', header: 'Address' },
      { field: 'dob', header: 'Date Of Birth' },
      { field: 'mob', header: 'Mobile' },
      { field: 'city', header: 'city' }
    ];

    this.totalRecords = this.employees.length;
  }
  openModal(targetModal, user, $event) {
    this.confirmationService.confirm({
      key: 'confirm-check',
      message: 'Are you sure that you want to Update this record?',
      accept: () => {
      this.update = true;
      this.modalService.open(targetModal, {
        centered: true,
        backdrop: 'static'
      });
      this.editProfileForm.patchValue({
        id: user.id,
        firstname: user.firstname,
        lastname: user.lastname,
        address: user.address,
        email: user.email,
        dob: user.dob,
        mob: user.mob,
        city: user.city
      });
    }
    });
  }
  openNewModal(targetModal) {
    this.update = false;
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static'
    });

    this.editProfileForm.patchValue({
      firstname: '',
      lastname: '',
      username: '',
      email: '',
      dob: '',
      mob: '',
      city: '',
      address: ''
    });
  }

  onSubmit() {

    this.modalService.dismissAll();
    
    if (this.update) {
      this.submitted = true;
    if (this.editProfileForm.invalid) {
        return;
    }
    this.loading = true;
      this.userService.update(this.editProfileForm.getRawValue())
        .pipe(first())
        .subscribe(
          data => {
            console.log("we got the data", data);
          },
          error => {
            // this.alertService.error(error);
          });
    } else {
      this.submitted = true;
    if (this.editProfileForm.invalid) {
        return;
    }
    this.loading = true;
      this.userService.createEmp(this.editProfileForm.getRawValue())
        .pipe(first())
        .subscribe(
          data => {
            console.log("we got the data", data);
            this.loadAllEmployees();
          },
          error => {
            // this.alertService.error(error);
            // this.loading = false;
          });
    }
  }
}